from Cryptodome.Cipher import AES
from Crypto.Util.Padding import pad
import binascii
import os
import testy
import random

##################################################
## Unique keys 
##################################################
random.seed(random.random())
key = os.urandom(16)
IV = os.urandom(16) 
mask = os.urandom(16)

##################################################
## Frame templates
##################################################
SAM_def = {
    "layerIdentifier": [0] * 1,
    "SPI": [0] * 4,
    "ipAddr": [0] * 4,
    # cryptCtx
    "encAlg": [0] * 4,
    "encKey": [0] * 64,
    "encMask": [0] * 16

}

Packet_def = {
    "SPI": [0] * 4,
    "SequenceNo": [0] * 4,
    "IV": [0] * 16,
    "Payload": [0] * 128,
    # "Padding" : "128",
    "Padding_len": [0],
    "Next_header": [0]
}

####################################################

def encrypt(plain_text, key, IV):  
    """
    # AES encryption procedure
    """
    plain_text = binascii.unhexlify(plain_text)
    encryptor = AES.new(key, AES.MODE_CBC, IV=IV)
    ciphertext = encryptor.encrypt(plain_text)
    result = binascii.hexlify(ciphertext).upper()
    return binascii.unhexlify(result)


def decrypt(ciphertext, key, IV):
    """
    # AES decryption procedure
    """
    enc = binascii.unhexlify(ciphertext)
    encryptor = AES.new(key, AES.MODE_CBC, IV=IV)
    text = encryptor.decrypt(enc)
    return binascii.hexlify(text).upper()

def string2hex(message):
    """
    # Simple conversion 
    """
    return binascii.hexlify(bytes(message, "utf8"))


def padding(hex):
    """
    # Add padding for encryption procedure.
    # AES require 128bits long blocks
    """
    v = 16 - (len(hex) % 16)
    if v > 0 and v < 16:
        return [(hex + b'\x30'*v), v]
    else:
        return [hex, 0]


def divideBy128bits(packet):
    """
    # Split data every 128 bit and return in list form.
    # 128 bits is size of AES block
    """
    divided = []
    tempDivided = []

    for i in range(0,len(packet)):
        tempDivided.append(packet[i])
        if (i + 1) % 16 == 0:
            divided.append(tempDivided)
            tempDivided = []
    return divided



def main():



    # First let us encrypt secret message
    encrypted = encrypt(testy.text, testy.key, testy.IV)
    #print(encrypted)

    # Let us decrypt using our original password
    decrypted = decrypt(encrypted, testy.key, testy.IV)
    #print(decrypted)





    text_input = "Securing Data Transmission: Cryptology, Watermarking and Steganography"
    bytes_text = bytearray(text_input, "utf8")
    bits = ""
    
    hexy = string2hex(text_input)

    #print((hexy))
    #print(padding(hexy))

    for byte in bytes_text:
        #print(bin(byte))
        bits += bin(byte)
    #print(bits)

    

if __name__ == "__main__":
    main()

