#!/usr/bin/env python3

import socket, select
import crypto
import binascii
import time

from copy import copy
from crypto import SAM_def
from crypto import Packet_def
SAM = copy(SAM_def)
Packet = copy(Packet_def)

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)
MAX_DEVICES = 10

DEBUG = False

existingSAM = {}

if __name__ == "__main__":

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, PORT))
        s.listen(MAX_DEVICES)
        inputs = [s]

        while True:
            infds, outfds, errfds = select.select(inputs, inputs, [], MAX_DEVICES)
            if len(infds) != 0:
                for fds in infds:
                    if fds is s:
                        conn, addr = fds.accept()
                        inputs.append(conn)

                    else:

                        start = time.time()

                        print(f'Connected by', addr)
                        recvFrame = conn.recv(2048)
                        try:
                            cli_sock = inputs[inputs.index(fds)]
                            if 45 == len(recvFrame):
                                SAM["layerIdentifier"] = recvFrame[0]
                                SAM["SPI"]  =  recvFrame[1:5]
                                SAM["ipAddr"] = recvFrame[5:9]
                                SAM["encAlg"] = recvFrame[9:13]
                                SAM["encKey"] = recvFrame[13:29]
                                SAM["encMask"] = recvFrame[29:45]

                                existingSAM[SAM["SPI"]] = SAM

                            elif len(recvFrame):
                                Packet["SPI"] = recvFrame[0:4]
                                Packet["SequenceNo"] = recvFrame[4:8]
                                Packet["IV"]= recvFrame[8:24]
                                Packet["Payload"] = recvFrame[24: -2]
                                Packet["Padding_len"] = recvFrame[-2:-1]
                                Packet["Next_header"] = recvFrame[-1::]


                                if DEBUG: print('Incomming: ', binascii.hexlify(Packet["Payload"]))
                                if DEBUG: print(existingSAM[Packet["SPI"]])
                                key = (existingSAM[Packet["SPI"]])["encKey"]
                                if DEBUG: print(key)
                                divided = crypto.divideBy128bits(Packet["Payload"])

                                if DEBUG: print("Packet[IV]: ", len(Packet["IV"]), Packet["IV"])
                                if DEBUG: print("crypto key: ",len(key), key)
                                if DEBUG: print("devided", divided)

                                toDecrypt = ""
                                paddingLen = int.from_bytes(Packet["Padding_len"], "big")
                                for i in range(len(divided)):
                                    num = int(i/8)
                                    pos = i%8
                                    value = SAM["encMask"][num]
                                    if DEBUG: print("value", value, "   ", value & (1 << pos))
                                    if value & (1 << pos):
                                        for num in divided[i]:
                                            toDecrypt += str(format(num,"02x"))

                                decrypted = crypto.decrypt(toDecrypt, key=key, IV=Packet["IV"])
                                decryptedBlock = crypto.divideBy128bits(binascii.unhexlify(decrypted))

                                if DEBUG: print("toDecrypt",(toDecrypt))
                                if DEBUG: print("Decrypted: ",decrypted)
                                if DEBUG: print("Decrypted and divided: ", decryptedBlock)

                                joined = []
                                n = 0
                                for i in range(len(divided)):
                                    num = int(i/8)
                                    pos = i % 8
                                    value = SAM["encMask"][num]
                                    if DEBUG: print("value", value, "   ", value & (1 << pos))
                                    if value & (1 << pos):

                                        joined.append(decryptedBlock[n])
                                        n+=1
                                    else:
                                        joined.append(divided[i])

                                result = ""
                                for i in range(len(joined)):
                                    for k in range(len(joined[i])):
                                        result += chr(joined[i][k])


                                if paddingLen > 0:
                                 result = result[:-paddingLen:]   #padding removal

                                stop = time.time(); 

                                if DEBUG: print("joined", joined)
                                print("\n\n-----------FINAL-----------\n\n")
                                print("Result: ", result)
                                #print("Time: ", stop - start, " sec")
                            if not recvFrame:
                                inputs.remove(fds)
                        except:
                            inputs.remove(fds)
