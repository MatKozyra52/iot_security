#!/usr/bin/env python3

import socket
import crypto
from base64 import b64encode
import sys
import binascii
import os
import time

from copy import copy
from crypto import SAM_def
from crypto import Packet_def
SAM = copy(SAM_def)
Packet = copy(Packet_def)


HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65432        # The port used by the server
DEBUG = True

########## Socket ##########
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

########## MAIN ##########
if __name__ == "__main__":

    start = time.time()
    
    ########## SAM ##########
    SAM["layerIdentifier"] = bytes("2", "utf-8")
    SAM["SPI"] = Packet["SPI"] = bytes(os.urandom(4))           #used in reciever to connect packet to coresponding mask
    SAM["ipAddr"] = bytes([127,0,0,1])
    SAM["encAlg"] = bytes([0,0,0,1])
    SAM["encKey"] = crypto.key
    SAM["encMask"] = crypto.mask

    if DEBUG : print("--> #DEBUG Argument is: ", sys.argv[1], type(sys.argv[1]))

    message = sys.argv[1]#"Hello 123456789 " * 20 # We can put data to send as argument during run
    padded = crypto.padding(bytes(message, "utf-8"))
    key = crypto.key
    divided = crypto.divideBy128bits(padded[0])

    if DEBUG:  print("--> #DEBUG message   ", message, "\n")
    #if DEBUG:  print("message_bytes   ", bytes(message, "utf-8"))
    #if DEBUG:  print("message_padded", padded[0])
    if DEBUG:  print("--> #DEBUG message_devided   ", len(divided), divided, "\n")
    if DEBUG:  print("--> #DEBUG message_hex   ", binascii.hexlify(padded[0]), "\n")


    selected = ""                   #selected for encryption with mask

    for i in range(len(divided)):
        num = int(i/8)
        pos = i%8
        value = crypto.mask[num]
        #if DEBUG:   print("value", value, "   ", value & (1 << pos))
        if value & (1 << pos):
            for num in divided[i]:
                selected += str(hex(num))[2:4]

    #if DEBUG:  print("maska   ", crypto.mask, "\n")
    if DEBUG:  print("--> #DEBUG selected   ", selected, "\n")

    encrypted = crypto.divideBy128bits(crypto.encrypt(str(selected),key=key,IV = crypto.IV))

    #if DEBUG:  print("maska", crypto.mask)
    #if DEBUG:  print("selected", selected)
    #if DEBUG:  print("encrypted", crypto.encrypt(selected,key=key,IV = crypto.IV))
    #if DEBUG:  print("encrypted_hex", binascii.hexlify(crypto.encrypt(selected,key=key,IV = crypto.IV)))
    if DEBUG:  print("--> #DEBUG encrypted_devided", encrypted, "\n")

    joined = []                     # encrypted payload with padding
    ivKey =[a for a in crypto.IV]

    #if DEBUG:  print("org_IV", binascii.hexlify(crypto.IV))
    #if DEBUG:  print(len(ivKey))

    #if DEBUG:  print("ivKey: ", len(ivKey), ivKey)
    #if DEBUG:  print("crypto key: ", len(key), key)
    n = 0
    for i in range(len(divided)):
        num = int(i/8)
        pos = i%8
        value = crypto.mask[num]
        #if DEBUG: print("value", value, "   ", value & (1 << pos))
        if value & (1 << pos):

            joined.append(encrypted[n])
            n+=1
        else:
            joined.append(divided[i])


    if DEBUG:  print("--> #DEBUG joined", len(joined), joined, "\n")

    IV_bit = bytearray(ivKey)
    #if DEBUG:  print("ivKEY", ivKey)
    #if DEBUG:  print("IV_bit", IV_bit)

    result = bytearray()
    for i in joined:
        result += bytearray(i)
    if DEBUG:  print("--> #DEBUG result", result, "\n")

    ########## PACKET ##########
    Packet["SPI"]
    Packet["SequenceNo"] = bytes([0,0,0,1])
    Packet["IV"]=IV_bit
    Packet["Payload"] = result
    Packet["Padding_len"] = bytes([padded[1]])
    Packet["Next_header"] = bytes("1", "utf-8")



    initFrame = b''
    for i in SAM:
        #if DEBUG:   print("SAM", SAM[i])
        initFrame += bytes(SAM[i])

    packetFrame = b''
    for i in Packet:
        #if DEBUG:   print("Packet", Packet[i])
        packetFrame += bytes(Packet[i])

   # if DEBUG:  print("result", result)
    if DEBUG:  print("--> #DEBUG packet", len(packetFrame), packetFrame, "\n")
    #if DEBUG:  print("initFrame", initFrame)

    print("Your message to send is: ", message, "\n")
    s.sendall(initFrame)
    print("Sent Init!")

    s.sendall(packetFrame)

    stop = time.time()

    print("Packet Sent")
    #print("Execution time", stop - start, " sec")
    input("Transmission is done. Press enter.")
    s.shutdown(socket.SHUT_RDWR)
    s.close()


